# NET-209

## Getting Started with Shared VPC

Hands-on environment

![](images/Picture1.png)

High level steps guide

## 1.   Create new AWS Organization and add member account(s)


a.  Create organization
         (https://docs.aws.amazon.com/organizations/latest/userguide/orgs_tutorials_basic.html)

b.  Enable all features in the organization
        (https://docs.aws.amazon.com/organizations/latest/userguide/orgs_manage_org_support-all-features.html)

c.  Verify email address

d.  Invite existing or new account to the org (hint: depends if you
        email server supports it, you may be able to use the
        email+number\@domain workaround to re-use same email)

## 2.   Create single VPC in the master account with the following setting:

<https://docs.aws.amazon.com/vpc/latest/userguide/working-with-vpcs.html>

a.  Allocation EIP to use with the NAT gateway

b.  Launch VPC wizard\> VPC with Public and Private subnets

c.  Create single public subnet with NAT gateway

d.  Two private subnets in different AZs

## 3.   Create AWS Directory Service in the private subnets and share with the member account(s):

<https://docs.aws.amazon.com/directoryservice/latest/admin-guide/ms_ad_getting_started_create_directory.html>

a.  AWS Managed Microsoft AD standard edition

b.  Choose your Shared VPC with two private subnets

c.  Note: creating MS directory can take over 15 minutes. So, you
        can proceed with other steps and get back to this latter

d.  Post creation: share the Directory with the member account

## 4.   Share subnet using Resource Access Manager (RAM):

<https://docs.aws.amazon.com/vpc/latest/userguide/vpc-sharing.html#vpc-sharing-share-subnet>

a.  Create new resource share

b.  Resource type: Subnets

c.  Choose the private subnets

d.  Uncheck the box to allow external accounts

e.  Add the 12 digits numbers for the member account

f.  Create the resource

## 5.   Optional: Create Service control Policy (SCP) to prevent member account from creating VPCs and apply it to the member account Launch Instance(s) in the share subnet:

<https://docs.aws.amazon.com/organizations/latest/userguide/orgs_manage_policies_scp.html>

a.  AWS Organizations\>Policies\>Create policy

b.  Provide policy name and description

c.  Policy statement:

        i.  Service: EC2

        ii. Action: CreateVPC

        iii. Resource: all EC2 resources

        iv. Create policy

> ![](images/Picture2.png)

d.  Enable the policy type on the root
    <https://docs.aws.amazon.com/organizations/latest/userguide/orgs_manage_policies.html#enable_policies_on_root>

e.  Attach the policy to the member account

f.  Login to the member account, try to create VPC.

<!-- -->

## 6.   Launch test instance and add to the AWS Directory domain:

<https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/LaunchingAndUsingInstances.html>

a.  Login to the member account, VPC\>view the shared VPC

b.  Launch new instance. You can add to the domain if the directory
        service is up and shared with the account. Otherwise, instance
        can be used to test Internet access through the NAT gateway.

c.  For Domain join to succeed, select an IAM role that has an
        "AmazonEC2RoleforSSM" policy attached

d.  For Security Group setting, allow all traffic from the VPC
        network CIDR.

## 7.   Connect to the instance using SSM session manager

<https://docs.aws.amazon.com/systems-manager/latest/userguide/systems-manager-setting-up.html>

a.  Systems Manager\>session manager\>start session

b.  Chose the test instance\>start session

c.  Check domain membership: Get-Wmiobject Win32\_ComputerSystem

        You should see Directory domain name if machine is joined to the
        domain

d.  Testing internet access: Ping 4.2.2.2
